package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Pais;
import org.springframework.data.repository.CrudRepository;

public interface PaisRepository extends CrudRepository<Pais,Integer> {
}
