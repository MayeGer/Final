package com.ucbcba.proyecto.entities;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

    @Entity
    @Table(name = "pais")
    public class Pais {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;

        @NotEmpty(message = "Debe ingresar una ciudad")
        @Size(max = 50,message = "Maximo 50")
        private String name;


        @OneToMany(mappedBy = "pais", cascade = CascadeType.ALL)
        private Set<User> users;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Set<User> getUsers() {
            return users;
        }

        public void setUsers(Set<User> users) {
            this.users = users;
        }
    }
