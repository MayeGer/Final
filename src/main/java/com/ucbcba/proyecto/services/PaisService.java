package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Pais;

public interface PaisService {

    Iterable<Pais> listAllPais();
    Pais getPaisById(Integer id);

    Pais savePais(Pais pais);

    void deletePais(Integer id);
}
