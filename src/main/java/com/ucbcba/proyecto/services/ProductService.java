package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Product;

public interface ProductService {

    Iterable<Product> listAllProduct();

    Product getProductById(Integer id);

    Product saveProduct(Product product);

    void deleteProduct(Integer id);
}
